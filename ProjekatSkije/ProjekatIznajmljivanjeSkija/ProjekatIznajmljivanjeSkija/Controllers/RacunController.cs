﻿using Microsoft.Ajax.Utilities;
using ProjekatIznajmljivanjeSkija.Models;
using ProjekatIznajmljivanjeSkija.Models.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjekatIznajmljivanjeSkija.Controllers
{
    public class RacunController : Controller
    {
        // GET: Racun
        Database bazaPodataka;

        public RacunController()
        {
            bazaPodataka = new Database();
        }
        public ActionResult Index(Racun racun)
        {            
            if(racun.ID==0)
            {
                ViewBag.racun = "";
            }
            else
            {
                ViewBag.racun = "nije prazno";
            }
            return View();
        }

        public ActionResult RacunPartial(Racun racun)
        {
            return PartialView("RacunPartial", racun);
        }

        [HttpPost]
        public ActionResult PretraziRacun()
        {
            string id = Request["txtsearch"];
            int ID;
            string button = Request["button"];
        if(button=="search")
            {
                if (int.TryParse(id, out ID))
                {
                    Racun racun = bazaPodataka.traziRacun(ID);
                    if (racun.ID == 0)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return RedirectToAction("Index", racun);
                    }
                }
                else
                {
                    TempData["UnosBroj"] = "U polju se mora upisati broj!";
                    return RedirectToAction("Index");

                }
            }
        else
            {
                return RedirectToAction("Index");
            }




        }

        [HttpPost]
        public ActionResult Storniranje()
        {
            int id = int.Parse(Request["id"]);
            bazaPodataka.StornirajRacun(id);
            TempData["storniranje"] = "Uspesno ste stornirali racun sa sifrom: " + id;
            return RedirectToAction("Index");
        }
    }
}