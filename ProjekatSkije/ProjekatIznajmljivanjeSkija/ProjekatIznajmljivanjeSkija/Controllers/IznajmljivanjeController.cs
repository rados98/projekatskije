﻿using ProjekatIznajmljivanjeSkija.Models;
using ProjekatIznajmljivanjeSkija.Models.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjekatIznajmljivanjeSkija.Controllers
{
    public class IznajmljivanjeController : Controller
    {
        // GET: Iznajmljivanje
        Database bazaPodataka;

        public IznajmljivanjeController()
        {
            bazaPodataka = new Database();
        }
        public ActionResult Index()
        {
            List<Proizvod> skije = bazaPodataka.getAllSkije();
            List<Proizvod> oprema = bazaPodataka.getAllOprema();
            ViewBag.skije = skije;
            ViewBag.oprema = oprema;
            return View();
        }
        [HttpPost]
        public ActionResult Iznajmi()
        {
            bool izabrano = true;
            int IDskije = int.Parse(Request.Form["skije"]);
            int IDoprema = int.Parse(Request.Form["oprema"]);
            if (IDskije == 0 && IDoprema == 0)
            {
                TempData["greskaStanje"] = "Morate izabrati bar jedan proizvod";
                izabrano = false;

            }
            else
            {
                bazaPodataka.stampajRacun(IDskije, IDoprema);
            }

            if(Database.greskaStranje!="")
            {
                TempData["greskaStanje"] = Database.greskaStranje;
            }

          else 
            {
                if (izabrano)
                {
                    TempData["uspeh"] = "Uspesno ste iznajmili svoje proizvode!";
                }
            }

            
            return RedirectToAction("Index");
        }
    }
}