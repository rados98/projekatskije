﻿using ProjekatIznajmljivanjeSkija.Models.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ProjekatIznajmljivanjeSkija.Models
{
    public class Database
    {
        public ProjekatSkijeEntities4 db = new ProjekatSkijeEntities4();
        public static string greskaStranje = "";
        public static string uspeh = "";
        public static float ukupnaVrednost = 0;


        

        public List<Proizvod> getAllSkije()
        {
            List<Proizvod> proizvodiUMagacinu = new List<Proizvod>();
            foreach (Proizvod s in db.Proizvod)
            {

                Magacin_proizvod ms = db.Magacin_proizvod.Where(t => t.IDProizvod == s.ID).FirstOrDefault();
                if (ms != null)
                {
                    s.stanjeUMagacinu = ms.Stanje;
                }
                else
                {
                    s.stanjeUMagacinu = 0;
                }
                proizvodiUMagacinu.Add(s);


            }

            List<Proizvod> skije = proizvodiUMagacinu.Where(t => t.Naziv == "Skije").Select(t => t).ToList();
            return skije;
        }


        public List<Proizvod> getAllOprema()
        {
            List<Proizvod> proizvodiUMagacinu = new List<Proizvod>();

            foreach (Proizvod s in db.Proizvod)
            {

                Magacin_proizvod ms = db.Magacin_proizvod.Where(t => t.IDProizvod == s.ID).FirstOrDefault();
                if (ms != null)
                {
                    s.stanjeUMagacinu = ms.Stanje;
                }
                else
                {
                    s.stanjeUMagacinu = 0;
                }
                proizvodiUMagacinu.Add(s);


            }
            List<Proizvod> oprema = proizvodiUMagacinu.Where(t => t.Naziv == "Oprema").Select(t => t).ToList();


            return oprema;
        }

        public Proizvod GetProizvodByID(int id)
        {
            Proizvod p = db.Proizvod.Where(t => t.ID == id).FirstOrDefault();
            return p;
        }

        public Magacin_proizvod GetMagacinProizvodByID(int id)
        {
            Magacin_proizvod p = db.Magacin_proizvod.Where(t => t.IDProizvod == id).FirstOrDefault();
            return p;
        }


   

        public void dodajIznajmljivanje()
        {
            DateTime date = DateTime.Now;
            TimeSpan time = DateTime.Now.TimeOfDay;
            Iznajmljivanje i = new Iznajmljivanje()
            {
                Datum = date,
                Vreme = time,
               
            };
            db.Iznajmljivanje.Add(i);
            db.SaveChanges();

        }

        public void iznajmi(int idskija, int idoprema)
        {
            Database.greskaStranje = "";
            Database.ukupnaVrednost = 0;
            dodajIznajmljivanje();
            Iznajmljivanje i = db.Iznajmljivanje.OrderByDescending(p => p.ID).FirstOrDefault();

            if (idskija != 0 )
            {
                
                Iznajmljivanje_magacin iznajmiSkije = new Iznajmljivanje_magacin();
                Magacin_proizvod skije = GetMagacinProizvodByID(idskija);
                if (skije.Stanje >= 1)
                {
                    iznajmiSkije.IDIznajmljivanja = i.ID;
                    iznajmiSkije.IDProizvoda = skije.IDProizvod;
                    iznajmiSkije.IDMagacin = skije.IDMagacin;
                    
                    db.Iznajmljivanje_magacin.Add(iznajmiSkije);
                    skije.Stanje--;
                    Database.ukupnaVrednost += Convert.ToSingle( skije.Proizvod.Cena);
                  
                    
                }
                else
                {
                    Database.greskaStranje += "---Odabranih skija nema na stanju";
                }
                
            }

            if(idoprema!=0)
            {
                Iznajmljivanje_magacin iznajmiOpremu = new Iznajmljivanje_magacin();
                Magacin_proizvod oprema = GetMagacinProizvodByID(idoprema);
                if (oprema.Stanje >= 1)
                {
                    iznajmiOpremu.IDIznajmljivanja = i.ID;
                    iznajmiOpremu.IDProizvoda = oprema.IDProizvod;
                    iznajmiOpremu.IDMagacin = oprema.IDMagacin;
                  
                    db.Iznajmljivanje_magacin.Add(iznajmiOpremu);
                    oprema.Stanje--;
                    Database.ukupnaVrednost += Convert.ToSingle(oprema.Proizvod.Cena);
                }
                else
                {
                    Database.greskaStranje += "---Odabrane opreme nema na stanju";
                }

            }

            db.SaveChanges();
           

        }

      

        public void dodajRacun(float ukupnaVrednost)
        {
            DateTime date = DateTime.Now;
            TimeSpan time = DateTime.Now.TimeOfDay;
            Racun noviRacun = new Racun()
            {
                Datum = date,
                Vreme = time,
                UkupnaVrednost = ukupnaVrednost
            };
            db.Racun.Add(noviRacun);
            db.SaveChanges();

        }
        public void stampajRacun(int idskija, int idoprema)
        {
            iznajmi(idskija, idoprema);
            dodajRacun(Database.ukupnaVrednost);
            Racun racun = db.Racun.OrderByDescending(p => p.ID).FirstOrDefault();
            Iznajmljivanje i = db.Iznajmljivanje.OrderByDescending(p => p.ID).FirstOrDefault();
            List<Iznajmljivanje_magacin> lista = db.Iznajmljivanje_magacin.Where(t => t.IDIznajmljivanja == i.ID).Select(t => t).ToList();
            
            foreach(Iznajmljivanje_magacin im in lista)
            {
                StavkaRacuna sr = new StavkaRacuna()
                {
                    IDRacun = racun.ID,
                    IDIznajmljivanja = im.IDIznajmljivanja,
                    IDProizvod = im.IDProizvoda,
                    Cena = im.Magacin_proizvod.Proizvod.Cena

                };
                db.StavkaRacuna.Add(sr);
                
            }
            db.SaveChanges();
            Database.uspeh = "uspesno!";



        }


        public Racun traziRacun(int id)
        {
            Racun racun = db.Racun.Where(t => t.ID == id).FirstOrDefault();
            if (racun != null)
            {
                return racun;
            }
            else
                return racun = new Racun();

        }


        public void StornirajRacun(int id)
        {
            Racun r = db.Racun.Where(t => t.ID == id).FirstOrDefault();
            r.Storniran = 1;
            db.SaveChanges();

        }
     
        }
    }