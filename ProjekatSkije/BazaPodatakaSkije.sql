USE [master]
GO
/****** Object:  Database [ProjekatSkije]    Script Date: 10.5.2020. 14:14:13 ******/
CREATE DATABASE [ProjekatSkije]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ProjekatSkije', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\ProjekatSkije.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ProjekatSkije_log', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\ProjekatSkije_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [ProjekatSkije] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ProjekatSkije].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ProjekatSkije] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ProjekatSkije] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ProjekatSkije] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ProjekatSkije] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ProjekatSkije] SET ARITHABORT OFF 
GO
ALTER DATABASE [ProjekatSkije] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ProjekatSkije] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ProjekatSkije] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ProjekatSkije] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ProjekatSkije] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ProjekatSkije] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ProjekatSkije] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ProjekatSkije] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ProjekatSkije] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ProjekatSkije] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ProjekatSkije] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ProjekatSkije] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ProjekatSkije] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ProjekatSkije] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ProjekatSkije] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ProjekatSkije] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ProjekatSkije] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ProjekatSkije] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ProjekatSkije] SET  MULTI_USER 
GO
ALTER DATABASE [ProjekatSkije] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ProjekatSkije] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ProjekatSkije] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ProjekatSkije] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ProjekatSkije] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ProjekatSkije] SET QUERY_STORE = OFF
GO
USE [ProjekatSkije]
GO
/****** Object:  Table [dbo].[Iznajmljivanje]    Script Date: 10.5.2020. 14:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Iznajmljivanje](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Datum] [date] NOT NULL,
	[Vreme] [time](7) NOT NULL,
 CONSTRAINT [PK_Iznajmljivanje] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Iznajmljivanje_magacin]    Script Date: 10.5.2020. 14:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Iznajmljivanje_magacin](
	[IDIznajmljivanja] [int] NOT NULL,
	[IDProizvoda] [int] NOT NULL,
	[IDMagacin] [int] NOT NULL,
 CONSTRAINT [PK_Iznajmljivanje_magacin] PRIMARY KEY CLUSTERED 
(
	[IDIznajmljivanja] ASC,
	[IDProizvoda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Magacin]    Script Date: 10.5.2020. 14:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Magacin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Grad] [nvarchar](50) NOT NULL,
	[Adresa] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Magacin] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Magacin_proizvod]    Script Date: 10.5.2020. 14:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Magacin_proizvod](
	[IDMagacin] [int] NOT NULL,
	[IDProizvod] [int] NOT NULL,
	[Stanje] [int] NOT NULL,
 CONSTRAINT [PK_Magacin_proizvod] PRIMARY KEY CLUSTERED 
(
	[IDMagacin] ASC,
	[IDProizvod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Proizvod]    Script Date: 10.5.2020. 14:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Proizvod](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Naziv] [nvarchar](50) NOT NULL,
	[Marka] [nchar](10) NOT NULL,
	[Velicina] [nvarchar](50) NOT NULL,
	[Cena] [float] NOT NULL,
 CONSTRAINT [PK_Proizvod] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Racun]    Script Date: 10.5.2020. 14:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Racun](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Datum] [date] NOT NULL,
	[Vreme] [time](7) NOT NULL,
	[UkupnaVrednost] [float] NOT NULL,
	[Storniran] [int] NOT NULL,
 CONSTRAINT [PK_Racun] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StavkaRacuna]    Script Date: 10.5.2020. 14:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StavkaRacuna](
	[IDRacun] [int] NOT NULL,
	[IDStavkaRacuna] [int] IDENTITY(1,1) NOT NULL,
	[IDIznajmljivanja] [int] NOT NULL,
	[IDProizvod] [int] NOT NULL,
	[Cena] [float] NOT NULL,
 CONSTRAINT [PK_StavkaRacuna] PRIMARY KEY CLUSTERED 
(
	[IDRacun] ASC,
	[IDStavkaRacuna] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Iznajmljivanje] ON 

INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (1, CAST(N'2020-05-03' AS Date), CAST(N'22:57:33.8745774' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (2, CAST(N'2020-05-03' AS Date), CAST(N'23:00:01.5660055' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (3, CAST(N'2020-05-03' AS Date), CAST(N'23:02:03.6559016' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (4, CAST(N'2020-05-03' AS Date), CAST(N'23:04:34.1813725' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (5, CAST(N'2020-05-03' AS Date), CAST(N'23:04:39.6203687' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (11, CAST(N'2020-05-09' AS Date), CAST(N'14:58:38.0147168' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (12, CAST(N'2020-05-09' AS Date), CAST(N'14:58:43.2005950' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (13, CAST(N'2020-05-09' AS Date), CAST(N'14:58:47.7550115' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (14, CAST(N'2020-05-09' AS Date), CAST(N'15:01:05.9950077' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (15, CAST(N'2020-05-09' AS Date), CAST(N'15:01:14.2301451' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (16, CAST(N'2020-05-09' AS Date), CAST(N'15:01:24.5858320' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (17, CAST(N'2020-05-09' AS Date), CAST(N'15:02:10.6878360' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (18, CAST(N'2020-05-09' AS Date), CAST(N'15:02:15.9710321' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (19, CAST(N'2020-05-09' AS Date), CAST(N'15:02:19.6862926' AS Time))
INSERT [dbo].[Iznajmljivanje] ([ID], [Datum], [Vreme]) VALUES (20, CAST(N'2020-05-09' AS Date), CAST(N'15:04:01.8041403' AS Time))
SET IDENTITY_INSERT [dbo].[Iznajmljivanje] OFF
GO
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (1, 1, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (1, 2, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (2, 1, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (2, 2, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (3, 1, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (3, 2, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (5, 1, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (5, 5, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (11, 1, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (12, 5, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (13, 5, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (14, 2, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (15, 5, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (16, 1, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (16, 2, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (17, 2, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (18, 1, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (19, 5, 1)
INSERT [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda], [IDMagacin]) VALUES (20, 5, 1)
GO
SET IDENTITY_INSERT [dbo].[Magacin] ON 

INSERT [dbo].[Magacin] ([ID], [Grad], [Adresa]) VALUES (1, N'Prijepolje', N'Adresa 2')
SET IDENTITY_INSERT [dbo].[Magacin] OFF
GO
INSERT [dbo].[Magacin_proizvod] ([IDMagacin], [IDProizvod], [Stanje]) VALUES (1, 1, 22)
INSERT [dbo].[Magacin_proizvod] ([IDMagacin], [IDProizvod], [Stanje]) VALUES (1, 2, 19)
INSERT [dbo].[Magacin_proizvod] ([IDMagacin], [IDProizvod], [Stanje]) VALUES (1, 3, 0)
INSERT [dbo].[Magacin_proizvod] ([IDMagacin], [IDProizvod], [Stanje]) VALUES (1, 5, 5)
GO
SET IDENTITY_INSERT [dbo].[Proizvod] ON 

INSERT [dbo].[Proizvod] ([ID], [Naziv], [Marka], [Velicina], [Cena]) VALUES (1, N'Skije', N'Marka1    ', N'45', 6000)
INSERT [dbo].[Proizvod] ([ID], [Naziv], [Marka], [Velicina], [Cena]) VALUES (2, N'Oprema', N'Marka2    ', N'L', 2000)
INSERT [dbo].[Proizvod] ([ID], [Naziv], [Marka], [Velicina], [Cena]) VALUES (3, N'Skije', N'Marka3    ', N'43', 3500)
INSERT [dbo].[Proizvod] ([ID], [Naziv], [Marka], [Velicina], [Cena]) VALUES (5, N'Oprema', N'Marka4    ', N'M', 5000)
SET IDENTITY_INSERT [dbo].[Proizvod] OFF
GO
SET IDENTITY_INSERT [dbo].[Racun] ON 

INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (1, CAST(N'2020-05-03' AS Date), CAST(N'22:57:33.9207605' AS Time), 8000, 1)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (2, CAST(N'2020-05-03' AS Date), CAST(N'23:00:01.6129522' AS Time), 8000, 1)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (3, CAST(N'2020-05-03' AS Date), CAST(N'23:02:03.6990254' AS Time), 8000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (4, CAST(N'2020-05-03' AS Date), CAST(N'23:04:34.2092740' AS Time), 0, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (5, CAST(N'2020-05-03' AS Date), CAST(N'23:04:39.6380694' AS Time), 11000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (6, CAST(N'2020-05-09' AS Date), CAST(N'13:18:46.7495165' AS Time), 0, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (7, CAST(N'2020-05-09' AS Date), CAST(N'13:18:49.3723795' AS Time), 0, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (8, CAST(N'2020-05-09' AS Date), CAST(N'14:39:28.7326160' AS Time), 0, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (9, CAST(N'2020-05-09' AS Date), CAST(N'14:52:06.3282020' AS Time), 0, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (10, CAST(N'2020-05-09' AS Date), CAST(N'14:52:11.6509055' AS Time), 0, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (11, CAST(N'2020-05-09' AS Date), CAST(N'14:58:38.1054707' AS Time), 6000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (12, CAST(N'2020-05-09' AS Date), CAST(N'14:58:43.2245170' AS Time), 5000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (13, CAST(N'2020-05-09' AS Date), CAST(N'14:58:47.7609630' AS Time), 5000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (14, CAST(N'2020-05-09' AS Date), CAST(N'15:01:06.0851989' AS Time), 2000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (15, CAST(N'2020-05-09' AS Date), CAST(N'15:01:14.2670443' AS Time), 5000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (16, CAST(N'2020-05-09' AS Date), CAST(N'15:01:24.5987694' AS Time), 8000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (17, CAST(N'2020-05-09' AS Date), CAST(N'15:02:10.7317091' AS Time), 2000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (18, CAST(N'2020-05-09' AS Date), CAST(N'15:02:15.9779048' AS Time), 6000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (19, CAST(N'2020-05-09' AS Date), CAST(N'15:02:19.6934412' AS Time), 5000, 0)
INSERT [dbo].[Racun] ([ID], [Datum], [Vreme], [UkupnaVrednost], [Storniran]) VALUES (20, CAST(N'2020-05-09' AS Date), CAST(N'15:04:03.4917783' AS Time), 5000, 0)
SET IDENTITY_INSERT [dbo].[Racun] OFF
GO
SET IDENTITY_INSERT [dbo].[StavkaRacuna] ON 

INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (3, 1, 3, 1, 6000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (3, 2, 3, 2, 2000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (5, 3, 5, 1, 6000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (5, 4, 5, 5, 5000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (11, 5, 11, 1, 6000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (12, 6, 12, 5, 5000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (13, 7, 13, 5, 5000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (14, 8, 14, 2, 2000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (15, 9, 15, 5, 5000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (16, 10, 16, 1, 6000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (16, 11, 16, 2, 2000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (17, 12, 17, 2, 2000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (18, 13, 18, 1, 6000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (19, 14, 19, 5, 5000)
INSERT [dbo].[StavkaRacuna] ([IDRacun], [IDStavkaRacuna], [IDIznajmljivanja], [IDProizvod], [Cena]) VALUES (20, 15, 20, 5, 5000)
SET IDENTITY_INSERT [dbo].[StavkaRacuna] OFF
GO
ALTER TABLE [dbo].[Racun] ADD  CONSTRAINT [DF_Racun_Storniran]  DEFAULT ((0)) FOR [Storniran]
GO
ALTER TABLE [dbo].[Iznajmljivanje_magacin]  WITH CHECK ADD  CONSTRAINT [FK_Iznajmljivanje_magacin_Iznajmljivanje] FOREIGN KEY([IDIznajmljivanja])
REFERENCES [dbo].[Iznajmljivanje] ([ID])
GO
ALTER TABLE [dbo].[Iznajmljivanje_magacin] CHECK CONSTRAINT [FK_Iznajmljivanje_magacin_Iznajmljivanje]
GO
ALTER TABLE [dbo].[Iznajmljivanje_magacin]  WITH CHECK ADD  CONSTRAINT [FK_Iznajmljivanje_magacin_Magacin_proizvod] FOREIGN KEY([IDMagacin], [IDProizvoda])
REFERENCES [dbo].[Magacin_proizvod] ([IDMagacin], [IDProizvod])
GO
ALTER TABLE [dbo].[Iznajmljivanje_magacin] CHECK CONSTRAINT [FK_Iznajmljivanje_magacin_Magacin_proizvod]
GO
ALTER TABLE [dbo].[Magacin_proizvod]  WITH CHECK ADD  CONSTRAINT [FK_Magacin_proizvod_Magacin] FOREIGN KEY([IDMagacin])
REFERENCES [dbo].[Magacin] ([ID])
GO
ALTER TABLE [dbo].[Magacin_proizvod] CHECK CONSTRAINT [FK_Magacin_proizvod_Magacin]
GO
ALTER TABLE [dbo].[Magacin_proizvod]  WITH CHECK ADD  CONSTRAINT [FK_Magacin_proizvod_Proizvod] FOREIGN KEY([IDProizvod])
REFERENCES [dbo].[Proizvod] ([ID])
GO
ALTER TABLE [dbo].[Magacin_proizvod] CHECK CONSTRAINT [FK_Magacin_proizvod_Proizvod]
GO
ALTER TABLE [dbo].[StavkaRacuna]  WITH CHECK ADD  CONSTRAINT [FK_StavkaRacuna_Iznajmljivanje_magacin] FOREIGN KEY([IDIznajmljivanja], [IDProizvod])
REFERENCES [dbo].[Iznajmljivanje_magacin] ([IDIznajmljivanja], [IDProizvoda])
GO
ALTER TABLE [dbo].[StavkaRacuna] CHECK CONSTRAINT [FK_StavkaRacuna_Iznajmljivanje_magacin]
GO
ALTER TABLE [dbo].[StavkaRacuna]  WITH CHECK ADD  CONSTRAINT [FK_StavkaRacuna_Racun] FOREIGN KEY([IDRacun])
REFERENCES [dbo].[Racun] ([ID])
GO
ALTER TABLE [dbo].[StavkaRacuna] CHECK CONSTRAINT [FK_StavkaRacuna_Racun]
GO
USE [master]
GO
ALTER DATABASE [ProjekatSkije] SET  READ_WRITE 
GO
